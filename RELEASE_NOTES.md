Release Notes
-------------
2.3.0
-----
* Models can now skip saving sub models. This can be useful for stopping unintended side effects.

2.2.0
-----
* Cursor now does a better job at returning proper model instances rather than Dynamic Models.

2.1.4
-----
Fixed some disabled methods (now enabled and working).

2.1.3
-----
* Fixed issue with the cursor sometimes picking up cached DynamicModels and failing the assetion.

2.1.2
-----
* Added missing "use" dependency for DBOptions (ORM).

2.1.1
-----
* Added missing "use" dependency for DBOptions (Model).

2.1.0
-----
* find() findOne() and findById() now support DBOptions.

2.0.0
-----
* [BC Break] The ORM will no longer create collections/tables JIT.

1.0.1
-----
* Added copyright header to the files.

1.0.0
-----
* Initial Release.
