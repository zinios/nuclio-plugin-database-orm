<?hh //partial
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\database\orm
{
	function setDynamic(Model $object,string $key,mixed $val):void
	{
		$object->$key=$val;
	}
	
	function getDynamic(Model $object,string $key):mixed
	{
		return $object->$key;
	}
	
	function pushDynamic(Model $object,string $key,mixed $val):void
	{
		$object->$key->add($val);
	}
}
