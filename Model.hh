<?hh //partial
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\database\orm
{
	require_once('globals.hh');
	
	use nuclio\core\
	{
		ClassManager,
		plugin\Plugin
	};
	use nuclio\plugin\database\
	{
		orm\ORM,
		common\DBRecord,
		common\DBQuery,
		common\DBFields,
		common\DBOptions
	};
	use nuclio\plugin\annotations\reader\Reader;
	use \ReflectionClass;
	
	<<
	__ConsistentConstruct,
	factory
	>>
	class Model
	{
		const ORM_KEY	='_orm';
		const RELATE_ANY='<any>';
		
		protected ORM $_ORM;
		protected Vector<string> $_fields;
		protected Vector<string> $_idFields;
		protected Map<string,mixed> $_annotations			=Map{};
		protected Vector<string> $_selfSubModelReferences	=Vector{};
		protected bool $_dirty								=true;
		protected ?string $_collection						=null;
		
		public static function create(?Map<string,mixed> $record=null):this
		{
			$instance=new static($record);
			$instance->flagDirty();
			return $instance;
		}
		
		public static function find(DBQuery $query, DBOptions $options=Map{}):Cursor
		{
			$ORM=ORM::getInstance();
			return $ORM->findWithModel(new static(),$query,$options);
		}
		
		public static function findOne(DBQuery $query, DBOptions $options=Map{}):?Model
		{
			$ORM=ORM::getInstance();
			return $ORM->findOneWithModel(new static(),$query,$options);
		}
		
		public static function findById(mixed $id, DBOptions $options=Map{}):?Model
		{
			$ORM=ORM::getInstance();
			return $ORM->findByIdWithModel(new static(),$id);
		}
		
		public static function delete(DBQuery $query):bool
		{
			$ORM=ORM::getInstance();
			return $ORM->deleteWithModel(new static(),$query);
		}
		
		public static function distinct(string $field):Vector<mixed>
		{
			$ORM=ORM::getInstance();
			return $ORM->distinctWithModel(new static(),$field);
		}
		
		public function __construct(/* HH_FIXME[4033] */...$args)
		{
			$this->_ORM			=ORM::getInstance();
			$this->_fields		=new Vector(null);
			$this->_idFields	=new Vector(null);
			$annotationReader	=Reader::getInstance($this);
			$this->_annotations	=$annotationReader->getAnnotations();
			
			$this->mapIDFields();
			
			$args=new Vector($args);
			if ($args->get(0) instanceof Map)
			{
				foreach($args->get(0) as $field => $fieldValue)
				{
					$this->saveField($field);
				}
				$this->setMany($args[0]);
			}
			$this->flagClean();
		}
		
		public function __toString()
		{
			return (string)$this->getCollection();
		}
		
		protected function mapIDFields():void
		{
			$reflection=new ReflectionClass(static::class);
			foreach ($reflection->getProperties() as $property)
			{
				if ($property->isPublic())
				{
					$name=$property->getName();
					$this->_fields[]=$name;
					if (substr($name,-2,2)=='Id'
					|| substr($name,-3,3)=='_id')
					{
						$this->saveIdField($name);
					}
				}
			}
		}
		
		public function getFields():Vector<string>
		{
			return $this->_fields;
		}
		
		public function getAnnotations():Map<string,mixed>
		{
			return $this->_annotations;
		}
		
		public function flagDirty():this
		{
			$this->_dirty=true;
			return $this;
		}
		
		public function flagClean():this
		{
			$this->_dirty=false;
			return $this;
		}
		
		public function isDirty():bool
		{
			return $this->_dirty;
		}
		
		public function isClean():bool
		{
			return !$this->_dirty;
		}
		
		public function getORM():ORM
		{
			return $this->_ORM;
		}
		
		public function save(bool $saveSubModels=true):Model
		{
			if ($this->isDirty())
			{
				$driver=$this->getORM()->getDataSource()->getDriver();
				$this->recordInstancesOfSelfInSubModel();
				if ($saveSubModels)
				{
					$this->saveSubModels();
				}
				$record=$this->getORM()->save($this);
				
				foreach ($record as $field=>$val)
				{
					if (!$this->isRelationalField($field))
					{
						$this->set($field,$val);
					}
				}
				// $this->setMany($record);
				$this->flagClean();
				
				// foreach ($this->_selfSubModelReferences as $field)
				// {
				// 	dump($this->getCollection());
				// 	$model=$this->get($field);
				// 	if ($model instanceof Model)
				// 	{
				// 		if (!is_null($model->getId()))
				// 		{
				// 			$model->save();
				// 		}
				// 		dump($model->getCollection());
				// 		$self=$model->get($this->getCollection());
				// 		if ($self instanceof Model)
				// 		{
				// 			dump($self->getCollection());
				// 			dump($self->getId());
				// 		}
						
				// 		dump($model->getId());
						
				// 	}
				// 	else
				// 	{
				// 		die('no longer a Model');
				// 	}
				// }
				$this->_selfSubModelReferences=Vector{};
				
			}
			return $this;
		}
		
		protected function saveSubModels():void
		{
			foreach ($this->_fields as $field)
			{
				if ($this->isRelationalField($field))
				{
					
					$val=$this->get($field);
					if ($val instanceof Model)
					{
						$this->flagClean();
						$val->save();
						$this->flagDirty();
					}
				}
			}
		}
		
		protected function hasSubModel(Model $model):bool
		{
			foreach ($this->_fields as $field)
			{
				$annotation=$this->_annotations->get($field);
				if ($annotation instanceof Map
				&& $annotation->containsKey('relate'))
				{
					$relates=$annotation->get('relate');
					if (!is_null($relates))
					{
						return (get_class($model)===$relates);
					}
				}
			}
			return false;
		}
		
		protected function recordInstancesOfSelfInSubModel()
		{
			foreach ($this->_fields as $field)
			{
				if ($this->isRelationalField($field))
				{
					$model=$this->get($field);
					if ($model instanceof Model && $model->hasSubModel($this))
					{
						$this->_selfSubModelReferences->add($field);
					}
				}
			}
		}
		
		// private function saveSubModelsRecursive():void
		// {
		// 	foreach ($this->_fields as $field)
		// 	{
		// 		if ($this->isRelationalField($field))
		// 		{
		// 			$val=$this->get($field);
		// 			if ($val instanceof Model)
		// 			{
		// 				$this->flagClean();
		// 				$val->save();
		// 				$this->flagDirty();
		// 			}
		// 		}
		// 	}
		// }
		
		public function getCollection():string
		{
			if (is_string($this->_collection))
			{
				return $this->_collection;
			}
			else
			{
				$class=$this->_annotations->get('class');
				if (!is_null($class))
				{
					invariant($class instanceof Map, 'must be a Map.');
					$this->_collection=(string)$class->get('collection');
					return $this->_collection;
				}
				else
				{
					throw new ORMException('No collection asigned to model.');
				}
			}
		}
		
		protected function saveField(string $key):Model
		{
			if (!$this->isValidField($key))
			{
				$this->_fields[]=$key;
			}
			return $this;
		}

		protected function saveIdField(string $name):Model
		{
			$newName=strlen($name)-2 |> substr($name,0,$$);
			if (!$this->isIdField($newName))
			{
				$this->_idFields[]=$newName;
			}
			return $this;
		}
		
		protected function isIdField(string $field):bool
		{
			$result=$this->_idFields->linearSearch($field);
			return ($result!==-1);
		}
		
		public function __call(string $method,array<mixed> $args):mixed
		{
			$args=new Vector($args);
			$matches=[];
			if (preg_match('/(set|get|delete|add|remove|push|pull)(.*)/',$method,&$matches))
			{
				if (substr($matches[1],0,3)==='set')
				{
					if (!$args->containsKey(0))
					{
						$args=new Vector(null);
					}
					$key=$matches[2];
					
					if (!$this->isValidField($key))
					{
						$key=lcfirst($key);
					}
					
					if ($this->isIdField($key))
					{
						return $this->set($key.'Id',$args[0]);
					}
					else
					{
						return $this->set($key,$args[0]);
					}
				}
				else if (substr($matches[1],0,3)==='get')
				{
					$key=$matches[2];
					if (!$this->isValidField($key))
					{
						$key=lcfirst($key);
					}
					return $this->get($key);
				}
				else if (substr($matches[1],0,4) |> in_array($$,['push','add']))
				{
					if (!$args->containsKey(0))
					{
						$args=new Vector(null);
					}
					$key=$matches[2];
					
					if (!$this->isValidField($key))
					{
						$key=lcfirst($key);
					}
					
					$newValue=$args[0];
					if (property_exists($this,$key))
					{
						$container=$this->get($key);
						if (!$container instanceof Vector)
						{
							$this->set($key,Vector{});
						}
						$this->push($key,$newValue);
						return $this;
					}
					else
					{
						$key.='s';
						if (property_exists($this,$key))
						{
							$container=$this->get($key);
							if (!$container instanceof Vector)
							{
								$this->set($key,Vector{});
							}
							$this->push($key,$container);
							return $this;
						}
					}
				}
			}
			throw new ORMException('The method "'.$method.'" on model "'.__CLASS__.'" does not exist and could not be automagically overloaded.');
		}
		
		protected function isValidField(string $key):bool
		{
			return ($this->_fields->linearSearch($key)!==-1);
		}
		
		public function set(string $key,mixed $val):Model
		{
			if (substr($key,-2,2)=='Id' && !$this->isIdField($key))
			{
				$this->saveIdField($key);
			}
			if ($this->isRelationalField($key))
			{
				/*
				I'm changing the behavior of this to assume the value is always correct.
				
				This smells of danger as any non-relational database driver may bork
				when attempting to perform fetching of the related records.
				
				Unfortunately at this time I can't think of a way to make this work
				in a sane way between both Relational and Non-Relational database drivers.
				- Tim
				 */
				/* 
				if (is_null($val)
				|| ($this->isValidRelationalField($key,$val) && $val instanceof Model)
				
				|| ($val instanceof Map && $val->containsKey(self::ORM_KEY))/)
				{
					setDynamic($this,$key,$val);
				}
				else
				{
					throw new ORMException(sprintf('Setting the field "%s" requires an active, valid & matching model to be passed to it. Got "%s" instead.',$key,$val));
				}
				*/
				setDynamic($this,$key,$val);
			}
			else
			{
				setDynamic($this,$key,$val);
			}
			$this->flagDirty();
			return $this;
		}
		
		public function setMany(Map<string,mixed> $keyVals):Model
		{
			foreach ($keyVals as $key=>$val)
			{
				$this->set($key,$val);
			}
			return $this;
		}
		
		public function get(string $key):mixed
		{
			if (property_exists($this,$key))
			{
				$mud=$this->isDirty();
				$val=getDynamic($this,$key);
				if (!$val instanceof Model && $this->isRelationalField($key))
				{
					if ($val instanceof Map && $val->containsKey(self::ORM_KEY))
					{
						$ORMRef=$val->get(self::ORM_KEY);
						if (!is_null($ORMRef)
						&& $ORMRef instanceof Map)
						{
							$class	=$ORMRef->get('class');
							$id		=$ORMRef->get('_id');
							if (!is_null($class) && !is_null($id))
							{
								$model=ClassManager::getClassInstance($class);
								$val=$model::findOne(Map{'id'=>$id});
								$this->set($key,$val);
							}
						}
					}
					else
					{
						$annotation=$this->_annotations->get($key);
						if ($annotation instanceof Map
						&& $annotation->containsKey('relate'))
						{
							$relates=$annotation->get('relate');
							if ($relates instanceof Vector)
							{
								$relationalClass=$relates->get(0);
								$relationalField=$relates->get(1);
								$model=ClassManager::getClassInstance($relationalClass);
								$_val=$model::findOne(Map{$relationalField=>$val});
								if (!is_null($_val))
								{
									$this->set($key,$_val);
								}
								//else use the default value.
							}
							else
							{
								$relationalClass=$relates;
								$relationalField='id';
								$model=ClassManager::getClassInstance($relationalClass);
								$_val=$model::findOne(Map{$relationalField=>$val});
								if (!is_null($_val))
								{
									$this->set($key,$_val);
									$val=$_val;
								}
								//else use the default value.
							}
						}
					}
				}
				// if ($this->isRelationalField($key)
				// && $val instanceof Map
				// && $val->containsKey(self::ORM_KEY))
				// {
				// 	$ORMRef=$val->get(self::ORM_KEY);
				// 	if (!is_null($ORMRef)
				// 	&& $ORMRef instanceof Map)
				// 	{
				// 		$class	=$ORMRef->get('class');
				// 		$id		=$ORMRef->get('_id');
				// 		if (!is_null($class) && !is_null($id))
				// 		{
				// 			$model=ClassManager::getClassInstance($class);
				// 			$val=$model::findOne(Map{'id'=>$id});
				// 			$this->set($key,$val);
				// 		}
				// 	}
				// }
				if (!$mud)
				{
					$this->flagClean();
				}
				return $val;
			}
			return null;
		}
		
		protected function push(string $key,mixed $val):Model
		{
			pushDynamic($this,$key,$val);
			return $this;
		}
		
		protected function isRelationalField(string $field):bool
		{
			$annotation=$this->_annotations->get($field);
			if ($annotation instanceof Map
			&& $annotation->containsKey('relate'))
			{
				$relates=$annotation->get('relate');
				return !is_null($relates);
			}
			return false;
		}
		
		protected function isValidRelationalField(string $field,mixed $val):bool
		{
			$annotation=$this->_annotations->get($field);
			if ($val instanceof Model
			&& $annotation instanceof Map
			&& $annotation->containsKey('relate'))
			{
				$relates=$annotation->get('relate');
				$class=get_class($val);
				if (!is_null($relates))
				{
					if ($relates instanceof Vector)
					{
						$relationalClass=$relates->get(0);
						if ($relationalClass==self::RELATE_ANY
						|| $class===$relationalClass)
						{
							return true;
						}
					}
					elseif ($relates==self::RELATE_ANY
					|| $class===$relates)
					{
						return true;
					}
				}
			}
			return false;
		}
		
		protected function fillFields():void
		{
			$reflection=new ReflectionClass($this);
			foreach ($reflection->getProperties() as $property)
			{

				if ($property->isProtected())
				{
					$name=$property->getName();
					$this->_fields[]=$name;
					if (substr($name,-2,2)=='Id'
					|| substr($name,-3,3)=='_id')
					{
						$this->saveIdField($name);
					}
				}
			}
		}
		
		public function toMap():DBRecord
		{
			$map=new Map(null);
			foreach ($this->_fields as $field)
			{
				$map->set($field,$this->get($field));
			}
			return $map;
		}
		
		public function toArray():array<string,mixed>
		{
			$array	=$this->toMap()
					->toArray()
					|>$this->toArrayRecursive($$);
			return $array;
		}
		
		public function toArrayWithRelations():array<string,mixed>
		{
			$array	=$this->toMapWithRelations()
					->toArray()
					|>$this->toArrayRecursive($$);
			return $array;
		}
		
		protected function toArrayRecursive(array<string,mixed> $array):array<string,mixed>
		{
			foreach ($array as $key=>$val)
			{
				if ($val instanceof Map)
				{
					$val		=$val->toArray();
					$array[$key]=$this->toArrayRecursive($val);
				}
				else if (is_array($val))
				{
					$array[$key]=$this->toArrayRecursive($val);
				}
			}
			return $array;
		}
		
		public function toMapWithRelations():DBRecord
		{
			$map=new Map(null);
			foreach ($this->_fields as $field)
			{
				$val=$this->get($field);
				if ($val instanceof Model)
				{
					$map->set
					(
						$field,
						$this->makeRelationalObject
						(
							(string)get_class($val),
							(string)$val->getCollection(),
							(string)$val->getId()
						)
					);
				}
				else if ($val instanceof Map)
				{
					$map->set
					(
						$field,
						$this->toMapWithRelationsRecursive($val)
					);
				}
				else if (is_array($val))
				{
					$map->set
					(
						$field,
						$this->toMapWithRelationsRecursive(new Map($val))
					);
				}
				else
				{
					$map->set($field,$this->get($field));
				}
			}
			return $map;
		}
		
		protected function toMapWithRelationsRecursive(Map<string,mixed> $object):Map<string,mixed>
		{
			foreach ($object as $key=>$val)
			{
				if ($val instanceof Model)
				{
					$object->set
					(
						$key,
						$this->makeRelationalObject
						(
							(string)get_class($val),
							(string)$val->getCollection(),
							(string)$val->getId()
						)
					);
				}
				else if ($val instanceof Map)
				{
					$object->set
					(
						$key,
						$this->toMapWithRelationsRecursive($val)
					);
				}
				else if (is_array($val))
				{
					$object->set
					(
						$key,
						$this->toMapWithRelationsRecursive(new Map($val))
					);
				}
				else
				{
					$object->set($key,$object->get($key));
				}
			}
			return $object;
		}
		
		protected function makeRelationalObject(string $class,string $collection,string $id):array<string,mixed>
		{
			return [
				self::ORM_KEY=>
				[
					'class'		=>$class,
					'collection'=>$collection,
					'_id'		=>$id
				]
			];
		}
	}
}
